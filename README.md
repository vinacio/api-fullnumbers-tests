# Testing API

Projeto que contém testes em uma API que converte números inteiros em sua versão por extenso. Os testes efetuados são testes de contrato para validar se as especificações do projeto são atendidas.

O projeto foi desenvolvido em python na versão 3.9 utilizando bibliotecas padrão e externas. As bibliotecas externas utilizadas foram pipenv, locust e scanapi, sendo a primeira com a intenção de controle de versão das dependências do projeto e montagem de um ambiente isolado.

Para a execução é necessário que o python na versão mínima 3.8 esteja instalada em conjunto com o pipenv. Para maiores detalhes sobre a instalação acessar o link do pipenv abaixo.
* [Locust](https://locust.io/)
* [Pipenv](https://pipenv.pypa.io/en/latest/)
* [ScanAPI](https://scanapi.dev/)

# Passos para a execução

1. Clonar o projeto e criar um ambiente virtual contento suas dependências. Para isso executar o seguinte comando dentro do diretório raiz:
```
  pipenv install
```

2. Ativar o Pipenv shell
```
  pipenv shell
```

3. Para a execução dos testes foi criado um scrip (run.sh). Com ele todos os testes serão executados e os respectivos outputs gerados na pasta reports. Para a execução do scrip executar os seguintes comandos:
```
  chmod +x run.sh
  ./run.sh
```

4. Para efetuar a execução isolada dos testes:

  4.1. Testes Funcionais:
  ```
     scanapi run ./functional-tests/scanapi_tests.yml -o reports/functional-tests.html
  ```

  4.2. Testes de Performance:
  ```
     locust -f ./load-tests/locustfile.py --config ./load-tests/.locust.conf --csv reports/load-tests --csv-full-history
  ```

  4.3. Testes com todos os tipos que requisições HTTP:
  ```
     python ./methods/methods.py
  ```

Melhorias:
- Facilitar as execuções isoladas com implementação do `Argparse`. Isso possibilitará não somente a execução isolada dos tipos de teste como também a inserção de diferentes ranges através de argumentos.
- Adição em algum CI para maior controle a cada build.

As análises efetuadas de ambas as aplicações podem ser observadas através do [summary](summary.md).
