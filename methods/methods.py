"""Checagem de métodos HTTP."""
from os import path, remove
from random import randint

from requests import Request, Session

# valores dentro e fora do range especificado
r_expexted = randint(-10000, 10000)
r_unexpected_pos = randint(10001, 15000)
r_unexpected_neg = randint(-15000, -10001)
random_number = {
    "esperado": r_expexted,
    "não esperado negativo": r_unexpected_neg,
    "não esperado positivo": r_unexpected_pos,
}

http_methods = {
    "GET": "get",
    "POST": "post",
    "PUT": "put",
    "DELETE": "delete",
    "PATCH": "patch",
    "HEAD": "head",
}

s = Session()


REPORT_PATH = './reports/http_methods.txt'
if path.exists(REPORT_PATH):
    remove(REPORT_PATH)


def log_writer(page, status_code):
    """Salvando o arquivo de log dos metodos."""
    with open(REPORT_PATH, 'a') as log_file:
        log_file.write(page + '\n' + status_code + '\n')


for (key_n, number) in random_number.items():
    for (key_m, method) in http_methods.items():
        url = f"http://challengeqa.staging.devmuch.io/{number}"
        prepped = s.prepare_request(Request(method, url))
        response = s.send(prepped)
        page_msg = f"Página acessada: {url}"
        status_code_msg = (
                f"O status code para o método {key_m} "
                f"com um valor {key_n} foi {response.status_code}"
                )
        print(page_msg + '\n' + status_code_msg + '\n')
        log_writer(page_msg, status_code_msg)
