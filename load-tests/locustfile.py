"""Stress Test para cenários dentro e fora do especificado."""
from random import choices, randint
from string import ascii_letters

from locust import between, task
from locust.contrib.fasthttp import FastHttpUser


class PerformanceAPI(FastHttpUser):
    """Test de Stress realizados em ambas aplicações."""

    wait_time = between(1, 5)

    @task
    def home_page(self):
        """Stress Test na página principal."""
        endpoint_ptbr = "http://challengeqa.staging.devmuch.io/"
        self.client.get(endpoint_ptbr)

    @task
    def api_specified_values_ptbr(self):
        """Stress Test com valores dentro do especificado.

        Aplicação em Português para o range [-10000, 10000].
        """
        specified_number = randint(-10000, 10000)
        endpoint_ptbr = "http://challengeqa.staging.devmuch.io/"
        self.client.get(f"{endpoint_ptbr}{specified_number}")

    @task
    def api_specified_values_en(self):
        """Stress Test com valores dentro do especificado.

        Aplicação em Inglês para o range [-10000, 10000].
        """
        specified_number = randint(-10000, 10000)
        endpoint_ptbr = "http://challengeqa.staging.devmuch.io/en/"
        self.client.get(f"{endpoint_ptbr}{specified_number}")

    @task
    def api_stressed_values_ptbr(self):
        """Stress Test com valores acima do especificado.

        Aplicação em Português para o range [10001, 15000].
        """
        specified_number = randint(10001, 15000)
        endpoint_ptbr = "http://challengeqa.staging.devmuch.io/"
        self.client.get(f"{endpoint_ptbr}{specified_number}")

    @task
    def api_stressed_values_en(self):
        """Stress Test para valores acima do especificado.

        Aplicação em Inglês para o range [10001, 15000].
        """
        specified_number = randint(10001, 15000)
        endpoint_ptbr = "http://challengeqa.staging.devmuch.io/en/"
        self.client.get(f"{endpoint_ptbr}{specified_number}")

    @task
    def api_stressed_string_ptbr(self):
        """Stress test com string na aplicaçãp em Português."""
        random_string = ''.join(choices(ascii_letters, k=5))
        endpoint_ptbr = "http://challengeqa.staging.devmuch.io/"
        self.client.get(f"{endpoint_ptbr}{random_string}")

    @task
    def api_stressed_string_en(self):
        """Stress test com string na aplicação em Inglês."""
        random_string = ''.join(choices(ascii_letters, k=5))
        endpont_ptbr = "http://challengeqa.staging.devmuch.io/en/"
        self.client.get(f"{endpont_ptbr}{random_string}")
