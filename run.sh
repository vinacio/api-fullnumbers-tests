#!/bin/bash

# creating the reports folder
mkdir -p ./reports

# functional tests
scanapi run ./functional-tests/scanapi_tests.yml -o reports/functional-tests.html

# non-functional tests
locust -f ./load-tests/locustfile.py --config ./load-tests/.locust.conf --csv reports/load-tests --csv-full-history

# methods siplaying on terminal
python ./methods/methods.py
