### Servidor HTTP somente para requisições GET
Através da realização de testes com alguns tipos de requisições, como por exemplo POST, PUT, DELETE, PATH e HEAD, verifica-se que as requisições POST e PUT se comportam como o esperado dando o retorno 405, porém a requisição HEAD possui retorno 200, o que não está no escopo do projeto. Também pode-se verificar que as requisições GET efetuadas para valores fora do range especificado possuem retorno 400, o que se encontra dentro das especificações.

### Exploratório nas requisições
Para a realização dos cenários de teste seguindo as especificações do range de utilização [-10000, 10000], o seguinte esquema foi efetuado com o pré-requisito de validar as chaves, valores e content type das requisiçoes:
* Teste nulo, sem número, ou seja, acessando a [página principal](http://challengeqa.staging.devmuch.io/) do projeto;
* Teste de unidade incluindo inteiros únicos, dezena, centena e milhar;
* Cenários abaixo do range especificado;
* Cenários acima do range especificado;
* Testes com strings.

Alguns comportamentos verificados encontram-se fora das especificações do projeto, são eles:
* Aplicação em Português:
  * O limite superior, 10000, possui valor na chave "extenso" em inglês "ten thousand", quando deveria ser "dez mil"
  * Status code para um número diferente de inteiro como 401, sendo o requisito 400.
  * Nos valores correspondentes aos números 14, 16, 17 e 19 o valor retornado como grafia foi catorze, dezasseis, dezassete e dezanove, o que não corresponde ao português brasileiro, quatorze, dezesseis, dezessete e dezenove, respectivamente. Uma das possibilidades dessa causa é o vocabulário ter sido utilizado com português de Portugal.
  * Utilizando um número não inteiro (float) a aplicação esta retornando 401 no status code, ao invés de 400 conforme especificado.
* Aplicação em Inglês:
  * O Limite inferior, -10000, possui um content type text/plain, quando deveria ser JSON.
  * O status code da requisição com um valor acima do limite (10001) deveria ter retorno 400, mas está retornando 401.
  * A chave da aplicação, seguindo a língua inglesa, se encontra como "full", porém conforme os requisitos, esta deveria ter retorno "extenso", o que não é observado.
  * Status code para um número diferente de inteiro como 401, sendo o requisito 400.
  * Status code para números fora dos limites especificados deveria ser 400, esta retornando 401.

### Performances
Os dados de performance levam em conta todas as requisições que foram efetuadas para cada endpoint. Verifica-se que a média de tempo de resposta é baixa, o que deve ser analisado partindo de onde estão sendo efetuadas as requisições. Outro fator que afeta as baixas nos tempos de resposta são os conteúdos que são devolvidos pela requisição. Em produção as médias de tempo de resposta oscilarão conforme a efetivação de novos usuários, sendo possível identificar picos onde a aplicação possui maior fluxo e se a mesma suporta o fluxo recebido, bem como aumentará o tempo de resposta média das requisições em espera.

### Observações\sugestões
Na aplicação foi possível identificar dois tipos de português, do Brasil e de Portugal, com isso seria valido disponibilizar ao usuário qual português a aplicação está utilizando.

As duas aplicações possuem a mesma função, a de retornar um número inteiro pela sua grafia por extenso. Isto poderia ser feito utilizando apenas uma aplicação com a possibilidade de troca de linguagem pelo usuário, facilitando o desenvolvimento e os testes a serem efetuados no mesmo, bem como mitigando erros relacionados a linguagem utilizada. Da forma como foram desenvolvidas são necessários testes de mesma estrutura em ambas aplicações.

Através de algum plugin de leitura das linguagens utilizadas pela aplicação, como por exemplo Wappalizer,  pode-se verificar que a mesma foi construída em flask versão 1.0.1 e python 3.7.9. Da forma como foi efetuado o deploy da aplicação o acesso através das ferramentas de desenvolvedor são muito fáceis, gerando falhas de segurança, embora seja uma aplicação simplista.

Uma última sugestão seria a inserção de testes unitários a cada build, com posterior execução de testes de integração e usabilidade antes de efetuar o deploy em produção, da forma que está existem muitos erros que poderiam ser pegos anda em desenvolvimento. Este processo pode ser feito através da adição de um CI.
